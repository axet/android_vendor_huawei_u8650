#
# hw/camera.msm7x27.so -> lib/libcamera.so
#
# 1) lib/libcamera.so depends on old JB code:
#
# android::Overlay::queueBuffer(void*) - _ZN7android7Overlay11queueBufferEPv
# android::Overlay::setCrop(unsigned int, unsigned int, unsigned int, unsigned int) - _ZN7android7Overlay7setCropEjjjj
# android::ISurface::BufferHeap::BufferHeap(unsigned int, unsigned int, int, int, int, unsigned int, unsigned int, android::sp<android::IMemoryHeap> const&) - _ZN7android8ISurface10BufferHeapC1EjjiiijjRKNS_2spINS_11IMemoryHeapEEE
# android::ISurface::BufferHeap::~BufferHeap() - _ZN7android8ISurface10BufferHeapD1Ev
#
# 2) we can create libcamera.so wrapper over liboemcamera.so
#

BUILD_OLD_LIBCAMERA := true

PRODUCT_PACKAGES += \
    libQcomUI \
    camera.msm7x27

PRODUCT_COPY_FILES += \
    frameworks/base/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \

# Camera
PRODUCT_COPY_FILES += \
    vendor/huawei/u8650/camera/liboemcamera.so:obj/lib/liboemcamera.so \
    vendor/huawei/u8650/camera/liboemcamera.so:system/lib/liboemcamera.so \
    vendor/huawei/u8650/camera/libcamerX.so:system/lib/libcamera.so \
    vendor/huawei/u8650/camera/libbindeX.so:system/lib/libbindeX.so \
    vendor/huawei/u8650/camera/libutilX.so:system/lib/libutilX.so \
    vendor/huawei/u8650/camera/libuX.so:system/lib/libuX.so \
    vendor/huawei/u8650/camera/libmmjpeg.so:system/lib/libmmjpeg.so \
    vendor/huawei/u8650/camera/libmmipl.so:system/lib/libmmipl.so \
    vendor/huawei/u8650/camera/libmmprocess.so:system/lib/libmmprocess.so \
    vendor/huawei/u8650/camera/mm-qcamera-testsuite-client:system/bin/mm-qcamera-testsuite-client \
    vendor/huawei/u8650/camera/mm-qcamera-test:system/bin/mm-qcamera-test \

