# Wifi
PRODUCT_COPY_FILES += \
    vendor/huawei/u8650/wifi/dhd.ko:system/wifi/dhd.ko \
    vendor/huawei/u8650/wifi/firmware.bin:system/wifi/firmware.bin \
    vendor/huawei/u8650/wifi/firmware_apsta.bin:system/wifi/firmware_apsta.bin \
    vendor/huawei/u8650/wifi/nvram.txt:system/wifi/nvram.txt \
    hardware/broadcom/wlan/bcmdhd/firmware/bcm4329/fw_bcm4329.bin:system/vendor/firmware/fw_bcm4329.bin \
    vendor/huawei/u8650/wifi/dhcpcd.conf:system/etc/dhcpcd/dhcpcd.conf \
    vendor/huawei/u8650/wifi/wpa_supplicant.conf:system/etc/wifi/wpa_supplicant.conf

# Wifi
PRODUCT_COPY_FILES += \
    vendor/huawei/u8650/wifi/nvram.txt:system/wifi/nvram.txt \
