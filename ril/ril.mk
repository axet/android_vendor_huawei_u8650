# RIL
PRODUCT_COPY_FILES += \
    vendor/huawei/u8650/ril/libril.so:system/lib/libril.so \
    vendor/huawei/u8650/ril/libril.so:obj/lib/libril.so \
    vendor/huawei/u8650/ril/libril-qc-1.so:system/lib/libril-qc-1.so \
    vendor/huawei/u8650/ril/libril-qcril-hook-oem.so:system/lib/libril-qcril-hook-oem.so \
    vendor/huawei/u8650/ril/liboncrpc.so:system/lib/liboncrpc.so \
    vendor/huawei/u8650/ril/libauth.so:system/lib/libauth.so \
    vendor/huawei/u8650/ril/libcm.so:system/lib/libcm.so \
    vendor/huawei/u8650/ril/libdiag.so:system/lib/libdiag.so \
    vendor/huawei/u8650/ril/libdll.so:system/lib/libdll.so \
    vendor/huawei/u8650/ril/libdsm.so:system/lib/libdsm.so \
    vendor/huawei/u8650/ril/libdss.so:system/lib/libdss.so \
    vendor/huawei/u8650/ril/libgsdi_exp.so:system/lib/libgsdi_exp.so \
    vendor/huawei/u8650/ril/libgstk_exp.so:system/lib/libgstk_exp.so \
    vendor/huawei/u8650/ril/libhwrpc.so:system/lib/libhwrpc.so \
    vendor/huawei/u8650/ril/libmmgsdilib.so:system/lib/libmmgsdilib.so \
    vendor/huawei/u8650/ril/libnv.so:system/lib/libnv.so \
    vendor/huawei/u8650/ril/libwmsts.so:system/lib/libwmsts.so \
    vendor/huawei/u8650/ril/libwms.so:system/lib/libwms.so \
    vendor/huawei/u8650/ril/libpbmlib.so:system/lib/libpbmlib.so \
    vendor/huawei/u8650/ril/libqueue.so:system/lib/libqueue.so \
    vendor/huawei/u8650/ril/libqmi.so:system/lib/libqmi.so \
    vendor/huawei/u8650/ril/qmuxd:system/bin/qmuxd \

