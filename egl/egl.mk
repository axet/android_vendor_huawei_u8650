# Graphics
PRODUCT_COPY_FILES += \
    vendor/huawei/u8650/egl/libgsl.so:system/lib/libgsl.so \
    vendor/huawei/u8650/egl/eglsubAndroid.so:system/lib/egl/eglsubAndroid.so \
    vendor/huawei/u8650/egl/libEGL_adreno200.so:system/lib/egl/libEGL_adreno200.so \
    vendor/huawei/u8650/egl/libGLESv1_CM_adreno200.so:system/lib/egl/libGLESv1_CM_adreno200.so \
    vendor/huawei/u8650/egl/libGLESv2_adreno200.so:system/lib/egl/libGLESv2_adreno200.so \
    vendor/huawei/u8650/egl/libq3dtools_adreno200.so:system/lib/egl/libq3dtools_adreno200.so \
    vendor/huawei/u8650/egl/libsc-a2xx.so:system/lib/libsc-a2xx.so \
    vendor/huawei/u8650/egl/yamato_pfp.fw:system/etc/firmware/yamato_pfp.fw \
    vendor/huawei/u8650/egl/yamato_pm4.fw:system/etc/firmware/yamato_pm4.fw \

